IP_P1=35.179.77.229
IP_P2=35.179.77.229
IP_P3=35.179.77.229

DOCKER_PORT_P1=10997
SERVER_NAMES=("S4MDEV-1" "S4MDEV-2" "S4MDEV-3")

SSH_PORT='2222'
SSH_USER='develop'

SSH_CONNECT_TIMEOUT=5
DOCKER_API_PORT=2375


ENVT=newdev

ID=182sdsc41fs6
#LINE FORMAT:  'local_name'  'remote_service:remote_service_port' 'path' 'protocol'

SERVICE_NAMES=(
  "${ENVT}.vpn"       "${ENVT}.vpn:80" '' 'http'
  'inform'         'inform:1234' '' 'http'
  'inform'         'inform:80' '/swagger' 'http'
#  'blank-lumen'    'blank-lumen:80' '/swagger/' 'http'
#  'blank'          'newdev-blank-service:80' '/swagger/' 'http'
  'bpe'            'bpe:80' '/swagger/' 'http'
  'config'         'newdev-config-service:80' '/swagger/' 'http'
  'content'        'content:80' '/swagger/' 'http'
  'crowdfunding'   'crowdfunding:80' '/swagger/' 'http'
  'document-templates' 'document-templates:80' '/swagger/' 'http'
  'file-mover'     'file-mover:80' '/swagger/' 'http'
  'file-uploader'  'file-uploader:80' '/swagger/' 'http'
  'json-responder' 'json-responder:80' '/' 'http'
  'localise'       'localise:80' '/swagger/' 'http'
  'main-front'     'main-front:80' '/' 'http'
  'media-converter'  'media-converter:80' '/swagger/' 'http'
  'messenger'      'messenger:80' '/swagger/' 'http'
  'mongo_01'       'mongo_01:27017' '' 'mongo'
  'mongo_02'       'mongo_02:27017' '' 'mongo'
  'mongo_03'       'mongo_03:27017' '' 'mongo'
  'mongo'          'mongo:27017' '' 'mongo'
  'mysql'          'mysql:3306' '' 'mysql'
  'notification'   'notification:80' '/swagger/' 'http'
  'oms'            'oms:80' '/swagger/' 'http'
  'payment-service' 'payment-service:80' '/swagger/' 'http'
  'pls'            'pls-v3:80' '/swagger/' 'http'
#'pls2'           'pls2:80' '/swagger/' 'http'
  'portainer'      'portainer_portainer:9000' '/' 'http'
  'postgresql'     'postgresql:5432' '' 'postgresql'
  'postgresql-slave' 'postgresql-slave:5432' '' 'postgresql'
  'products'       'products:80' '/swagger/' 'http'
  'rabbit-web'     'rabbitmq:15672' '/' 'http'
  'rds'            'rds:80' '/swagger/' 'http'
  'redis'          'redis:6379' '' 'redis'
  'redis-web-interface' 'redis-web-interface:8081' '/' 'http'
  'sphinx'         'sphinx:9306' '' 'sphinx'
  'sso'            'sso:80' '/swagger/' 'http'
  'seats-manager'  'seats-manager:80' '/swagger/' 'http'
  'statistic'      'statistic:80' '/swagger/' 'http'
#  'test-platform'  'test-platform:80' '/' 'http'
  'ticketing'       'ticketing:80' '/swagger/'   'http'
  'ums'            'ums:80' '/swagger/' 'http'
#  'ups'            'ups:80' '/swagger/' 'http'
  'rabbitmq_01'       'rabbitmq_01:5672' '' 'rabbit'
  'rabbitmq_02'       'rabbitmq_02:5672' '' 'rabbit'
  'rabbitmq_03'       'rabbitmq_03:5672' '' 'rabbit'
  'system-service' 'system-service:80' '/swagger/' 'http'
  'payment-service-2' 'payment-service-2:80' '/swagger/' 'http'
  'uptime-kuma' 'uptime-kuma:3001' '/status/dev/' 'http'
)
